"use strict";
/*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
const gREQUEST_STATUS_OK = 200;
const gREQUEST_READY_STATUS_FINISH_AND_OK = 4;
const gBASE_URL = "http://203.171.20.210:8080/devcamp-lucky-dice";
const gREQUEST_HEADER = "application/json;charset=UTF-8";

/*** REGION 2 - Vùng gán / thực thi sự kiện cho các elements */
// gán sự kiện click button New Dice
$(document).on("click", "#btn-new-dice", onBtnNewDiceClick);

// gán sự kiện click button Dice Hisory
$(document).on("click", "#btn-dice-history", onBtnDiceHistoryClick);
// gán sự kiện click button Voucher Hisory
$(document).on("click", "#btn-voucher-history", onBtnVoucherHistoryClick);
// gán sự kiện click button Prize Hisory
$(document).on("click", "#btn-prize-history", onBtnPrizeHistoryClick);

/*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
// function xử lý sự kiện click button New Dice
function onBtnNewDiceClick() {
    // khai báo đối tượng chứa dữ liệu
    let vUserData = {
        username: "",
        firstname: "",
        lastname: ""
    };
    // B1: Thu thập dữ liệu
    getUserData(vUserData);
    // B2: Kiểm tra dữ liệu
    let vDataValid = checkUserData(vUserData);
    if (vDataValid) {
        // B3: Gọi API để tung xúc xắc
        let vXmlHttpNewDice = new XMLHttpRequest();
        callApiNewDice(vUserData, vXmlHttpNewDice);
        vXmlHttpNewDice.onreadystatechange = function() {
            if (this.readyState == gREQUEST_READY_STATUS_FINISH_AND_OK && this.status == gREQUEST_STATUS_OK) {
                // B4: Xử lý hiển thị
                console.log(vXmlHttpNewDice.responseText);
                displayDiceResult(vXmlHttpNewDice);
            }
        }
    }
}

// function xử lý sự kiện click button Dice History
function onBtnDiceHistoryClick() {
    // khai báo đối tượng chứa dữ liệu
    let vUserData = {
        username: "",
        firstname: "",
        lastname: ""
    };
    // B1: Thu thập dữ liệu
    getUserData(vUserData);
    // B2: Kiểm tra dữ liệu
    let vDataValid = checkUserData(vUserData);
    if (vDataValid) {
        // B3: Gọi API để lấy lịch sử tung xúc xắc
        let vXmlHttpDiceHistory = new XMLHttpRequest();
        callApiDiceHistory(vUserData, vXmlHttpDiceHistory);
        vXmlHttpDiceHistory.onreadystatechange = function() {
            if (this.readyState == gREQUEST_READY_STATUS_FINISH_AND_OK && this.status == gREQUEST_STATUS_OK) {
                // B4: xử lý hiển thị
                displayDiceHistoryToTable(vXmlHttpDiceHistory);
            }
        }
    }
}

// function xử lý sự kiện click button Voucher History
function onBtnVoucherHistoryClick() {
    // khai báo đối tượng chứa dữ liệu
    let vUserData = {
        username: "",
        firstname: "",
        lastname: ""
    };
    // B1: Thu thập dữ liệu
    getUserData(vUserData);
    // B2: Kiểm tra dữ liệu
    let vDataValid = checkUserData(vUserData);
    if (vDataValid) {
        // B3: Gọi API để lấy lịch sử Voucher
        let vXmlHttpVoucherHistory = new XMLHttpRequest();
        callApiVoucherHistory(vUserData, vXmlHttpVoucherHistory);
        vXmlHttpVoucherHistory.onreadystatechange = function() {
            if (this.readyState == gREQUEST_READY_STATUS_FINISH_AND_OK && this.status == gREQUEST_STATUS_OK) {
                // B4: xử lý hiển thị
                displayVoucherHistoryToTable(vXmlHttpVoucherHistory);
            }
        }
    }
}

// function xử lý sự kiện click button Prize History
function onBtnPrizeHistoryClick() {
    // khai báo đối tượng chứa dữ liệu
    let vUserData = {
        username: "",
        firstname: "",
        lastname: ""
    };
    // B1: Thu thập dữ liệu
    getUserData(vUserData);
    // B2: Kiểm tra dữ liệu
    let vDataValid = checkUserData(vUserData);
    if (vDataValid) {
        // B3: Gọi API để lấy lịch sử Prize
        let vXmlHttpPrizeHistory = new XMLHttpRequest();
        callApiPrizeHistory(vUserData, vXmlHttpPrizeHistory);
        vXmlHttpPrizeHistory.onreadystatechange = function() {
            if (this.readyState == gREQUEST_READY_STATUS_FINISH_AND_OK && this.status == gREQUEST_STATUS_OK) {
                // B4: xử lý hiển thị
                displayPrizeHistoryToTable(vXmlHttpPrizeHistory);
            }
        }
    }
}

/*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/
// function thu thập dữ liệu người dùng điền trên form
// funtion trả về đối tượng paramUserData được tham số hóa
function getUserData(paramUserData) {
    paramUserData.username = $("#input-username").val().trim();
    paramUserData.firstname = $("#input-firstname").val().trim();
    paramUserData.lastname = $("#input-lastname").val().trim();
}

// function kiểm tra dữ liệu
// function return true nếu tất cả dữ liệu hợp lệ, return false nếu có dữ liệu không hợp lệ
function checkUserData(paramUserData) {
    if (paramUserData.username == "") {
        alert("Vui lòng nhập username");
        return false;
    }
    else if (paramUserData.firstname == "") {
        alert("Vui lòng nhập firstname");
        return false;
    }
    else if (paramUserData.lastname == "") {
        alert("Vui lòng nhập lastname");
        return false;
    }
    return true;
}

// function gọi API để tung xúc xắc
function callApiNewDice(paramUserData, paramXmlHttp) {
    // chuyển đối tượng gửi đi sang dạng text
    let vStringUserData = JSON.stringify(paramUserData);
    paramXmlHttp.open("POST", gBASE_URL + "/dice", true);
    paramXmlHttp.setRequestHeader("Content-Type", gREQUEST_HEADER);
    paramXmlHttp.send(vStringUserData);
}

// function xử lý hiển thị kết quả New Dice
function displayDiceResult(paramXmlHttp) {
    let vResponseObj = JSON.parse(paramXmlHttp.responseText);
    let vDiceResult = vResponseObj.dice;
    // thay hình ảnh kết quả tương ứng số mặt xúc xắc
    $("#img-dice").attr("src", `images/${vDiceResult}.png`);
    // hiển thị thông điệp
    $("#h4-message").css("visibility", "visible");
    // hiển thị thông điệp tương ứng với số mặt xúc xắc
    if (vDiceResult > 3) {
        $("#h4-message").html("Chúc mừng bạn được điểm cao");
    }
    else {
        $("#h4-message").html("Chúc bạn may mắn hơn lần sau");
    }
    // Hiển thị voucher nếu có
    let vVoucherElement = $("#p-voucher");
    if (vResponseObj.voucher != null) {
        vVoucherElement
            .css("visibility", "visible")
            .html("<b>Voucher:</b>")
            .append("<br>" + vResponseObj.voucher.id)
            .append("<br>" + vResponseObj.voucher.phanTramGiamGia + "%");
    }
    else {
        vVoucherElement.css("visibility", "hidden");
    }
    // Hiển thị prize nếu có
    let vPrizeElement = $("#img-prize");
    if (vResponseObj.prize == "Xe máy") {
        vPrizeElement
            .css("visibility", "visible")
            .attr("src", "images/xe-may.jpg");
    }
    else if (vResponseObj.prize == "Sổ") {
        vPrizeElement
            .css("visibility", "visible")
            .attr("src", "images/so.jpg");
    }
    else if (vResponseObj.prize == "Áo mưa") {
        vPrizeElement
            .css("visibility", "visible")
            .attr("src", "images/ao-mua.jpg");
    }
    else if (vResponseObj.prize == "Bút") {
        vPrizeElement
            .css("visibility", "visible")
            .attr("src", "images/but.jpg");
    }
    else if (vResponseObj.prize == "Mũ") {
        vPrizeElement
            .css("visibility", "visible")
            .attr("src", "images/mu.jpg");
    }
    else {
        vPrizeElement.css("visibility", "hidden");
    }
}

// function gọi API lấy lịch sử tung xúc xắc
function callApiDiceHistory(paramUserData, paramXmlHttp) {
    paramXmlHttp.open("GET", gBASE_URL + "/dice-history?username=" + paramUserData.username, true);
    paramXmlHttp.send();
}

// function gọi API lấy lịch sử Voucher
function callApiVoucherHistory(paramUserData, paramXmlHttp) {
    paramXmlHttp.open("GET", gBASE_URL + "/voucher-history?username=" + paramUserData.username, true);
    paramXmlHttp.send();
}

// function gọi API lấy lịch sử Prize
function callApiPrizeHistory(paramUserData, paramXmlHttp) {
    paramXmlHttp.open("GET", gBASE_URL + "/prize-history?username=" + paramUserData.username, true);
    paramXmlHttp.send();
}

// function hiển thị dữ liệu Dice History lên table
function displayDiceHistoryToTable(paramXmlHttp) {
    let vDiceHistory = JSON.parse(paramXmlHttp.responseText).dices;
    // hiển thị table
    $("#div-table-container").css("display", "block");
    // Đổi giá trị table head tương ứng với Dice History
    let vTableHeaderRow = $("#table-head tr");
    // Đổi giá trị table head
    vTableHeaderRow.find("th:eq(0)").html("Game");
    vTableHeaderRow.find("th:eq(1)").html("Result");
    // clear data table body
    $("#table-body").html("");
    // insert data vào table
    for (let bIndex = 0; bIndex < vDiceHistory.length; bIndex ++) {
        // tạo dòng mới
        let vNewRow = $("<tr>").appendTo($("#table-body"));
        // gán data cho table
        vNewRow.append(
            $("<td>").text(bIndex + 1),
            $("<td>").text(vDiceHistory[bIndex])
        )
    }
}

// function hiển thị dữ liệu Voucher History lên table
function displayVoucherHistoryToTable(paramXmlHttp) {
    let vVoucherHistory = JSON.parse(paramXmlHttp.responseText).vouchers;
    // hiển thị table
    $("#div-table-container").css("display", "block");
    // Đổi giá trị table head tương ứng với Voucher History
    let vTableHeaderRow = $("#table-head tr");
    // Đổi giá trị table head
    vTableHeaderRow.find("th:eq(0)").html("Mã Voucher");
    vTableHeaderRow.find("th:eq(1)").html("Giảm Giá");
    // clear data table body
    $("#table-body").html("");
    // insert data vào table
    for (let bIndex = 0; bIndex < vVoucherHistory.length; bIndex ++) {
        // tạo dòng mới
        let vNewRow = $("<tr>").appendTo($("#table-body"));
        // gán data cho table
        vNewRow.append(
            $("<td>").text(vVoucherHistory[bIndex].id),
            $("<td>").text(vVoucherHistory[bIndex].phanTramGiamGia + "%")
        )
    }
}

// function hiển thị dữ liệu Prize History lên table
function displayPrizeHistoryToTable(paramXmlHttp) {
    let vPrizeHistory = JSON.parse(paramXmlHttp.responseText).prizes;
    // hiển thị table
    $("#div-table-container").css("display", "block");
    // Đổi giá trị table head tương ứng với Prize History
    let vTableHeaderRow = $("#table-head tr");
    // Đổi giá trị table head
    vTableHeaderRow.find("th:eq(0)").html("Game");
    vTableHeaderRow.find("th:eq(1)").html("Prize");
    // clear data table body
    $("#table-body").html("");
    // insert data vào table
    for (let bIndex = 0; bIndex < vPrizeHistory.length; bIndex ++) {
        // tạo dòng mới
        let vNewRow = $("<tr>").appendTo($("#table-body"));
        // gán data cho table
        vNewRow.append(
            $("<td>").text(bIndex + 1),
            $("<td>").text(vPrizeHistory[bIndex])
        )
    }
}